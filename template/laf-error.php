<?php
if(!headers_sent()):
?>

<html>
<head>
    <title>The site is experiencing error</title>
</head>
<body>
<?php
endif;
?>
<style>
    .container{
        max-width: 540px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 12px;
        padding-right: 12px;
    }
    .error-div{
        max-width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        font-family: -apple-system, sans-serif, 'Roboto', Calibri;
        color: #6610f2;

    }
    .error-div h1{
        vertical-align: middle;
    }
    @media screen and (min-width: 576px) {
        .container{
            max-width: 540px;
        }
    }
    @media screen and (min-width: 768px) {
        .container{
            max-width: 720px;
        }
    }
    @media screen and (min-width: 991px) {
        .container{
            max-width: 960px;
        }
    }
</style>
<div class="container">
    <?php if(DEBUG === true): ?>
        <div class="error-div">
            <h1 style="display: block">The site is experiencing error.</h1>
            <div style="width: 100%; color:#3c3c3b">
                <h2 style="font-size: 1rem; text-align: left">Error details:</h2>
                <p style="max-height: 10rem; overflow: auto">
                    <?php print_r($error); ?>
                </p>
            </div>
        </div>
    <?php
    else: ?>
        <div class="error-div">
            <h1 style="display: block">The site is experiencing error, that's all we could tell.</h1>
        </div>
    <?php
    endif;
    ?>
</div>


</body>
</html>
