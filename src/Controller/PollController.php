<?php

namespace Controller;

use Labforum;
use Labforum\OutputTools;
use Labforum\Request;
use Labforum\Account;
use Labforum\Thread;

class PollController{
    protected $query_var;
    public $connection;

    public function __construct()
    {
        global $connection;
        $this->query_var = Request::get_query_var_array();
        $this->connection =& $connection;

    }

    public function canVote(): bool
    {
        $thread_id = $this->query_var['thread_id'];

        $thread = new Thread();
        $account = new Account();

        $thread->setQueryingThreadId($thread_id);

        $permission = $thread->getReadPermission();

        if(!$account->getCurrentUserId()){
            $output_tools = new OutputTools();
            $output_tools->setErrorMessage('You have no rights to poll.');
            $output_tools->outputData('json', 403);
        }
    }
}