<?php


namespace Labforum;


class Meta
{
    public static $connection;
    public function __construct()
    {
        global $connection;
        Meta::$connection =& $connection;
    }

    /**
     * Convert an array and store meta as JSON in db.
     * @param array $meta_array
     */
    public function storeMeta($key, $meta_array)
    {
        $meta = json_encode($meta_array);
        $this::$connection->insert(PREFIX . '_meta', [
            'meta_key' => $key,
            'value' => $meta,
        ]);
    }

    public function getMeta($key)
    {
        $data = $this::$connection->select(PREFIX . '_meta', '*', [
            'meta_key[=]' => $key
        ]);
        return (!empty($data)) ? json_decode($data) : '';
    }

}