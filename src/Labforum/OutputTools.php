<?php

namespace Labforum;

class OutputTools {

    public $data = array();

    public function __construct()
    {

    }

    /**
     * Since 4.0
     * setErrorMessage()
     * Set error message for output. Set error message as true if message is empty
     *
     * @param bool $message
     */
    public function setErrorMessage($message = true)
    {
        $this->data['error'] = $message;
    }

    /**
     * Since 4.0
     * setSuccessMessage()
     * Set success message for output. Set error message as true if message is empty
     *
     * @param bool $message
     */
    public function setSuccessMessage($message = true)
    {
        unset($this->data['error']);
        $this->data['success'] = $message;
    }

    public function setData($data){
        $this->data = $data;
    }

    /**
     * Since 4.0
     * outputData()
     * Output data according to type.
     *
     * @param string $type    Supported type: JSON
     * @param int $http_response_code
     */

    public function outputData($type = 'json', $http_response_code = 200)
    {
        if(empty($this->data['error'])){
            $http_response_code = 200;
        }
        http_response_code($http_response_code);
        switch ($type) {
            case 'json' :
                header('Content-Type: application/json');
                print_r(json_encode($this->data));
                exit;
        }
    }

}