<?php

namespace Labforum;

class Router
{
    private static $routes = array();
    private static $preg_reserved_matching_char = array('(', ')');
    private static $pattern_stack = array();
    private static $query_vars = array();
    private static $controllers = array();
    public function __construct()
    {
    }

    /**
     * Since 4.0
     * flattenControllerArr()
     * The ControllerArr might have different levels. We use this function to flatten
     * all controller in order to be executed
     *
     * @param array $arr  The controller list to be flattened
     * @return array
     */

    protected static function flattenControllerArr($arr = null) : array
    {
        if (!is_array($arr) || empty($arr)) {
            return array();
        }
        $result = array();
        foreach ($arr as  $key => $val) {
            if (is_array($val)) {
                $result = array_merge($result, self::flattenControllerArr($val));
            }
            else {
                $result = array_merge($result, array($key => $val));
            }
        }
        return $result;
    }

    /**
     * getPregPattern()  - Replace pattern with regex pattern
     * @param $pattern
     * @param $regex_syntax
     * @return string
     *
     */
    protected static function getPregPattern($pattern, $regex_syntax) : string
    {

        $str_replaced_matching_char = str_replace(self::$preg_reserved_matching_char, '', $pattern);
        preg_match_all('/{(.*?)}/', $str_replaced_matching_char, $matching_haystack);

        //The total matching haystack length is equal to the matches length - first element
        $matching_haystack_len = count($matching_haystack) - 1;
        for($i =0; $i < $matching_haystack_len; $i++) {
            //Add matching wrapper to regex syntax
            $current_regex_syntax = '(' . (is_string($regex_syntax) ? $regex_syntax : $regex_syntax[$i]) . ')';
            $str_replaced_matching_char = preg_replace('/{(.*?)}/', $current_regex_syntax, $str_replaced_matching_char);
        }
        return $str_replaced_matching_char;
    }

    /**
     * Since 4.0
     * parent() - Use parent to simplify writing
     * @param string $pattern Regex Pattern
     * @param array|Closure $callback
     * @param Closure $closure
     * @param array|string $regex_syntax
     */
    public static function parent($pattern, $callback, $closure = '', $regex_syntax = '[\w-]+?')
    {
        //Push parent pattern into stack
        $preg_pattern = self::getPregPattern($pattern, $regex_syntax);
        array_push(self::$pattern_stack, ltrim(rtrim($preg_pattern, '/'), '/'));
        //Push parent pattern variable name into stack
        preg_match_all("/{(.*?)}/", $pattern, $matches);
        foreach($matches[1] as $match_item) {
            array_push(self::$query_vars, $match_item);
        }

        //Push controller into stack
        if(is_array($callback)){
            array_push(self::$controllers, $callback);
        }else{
            $closure = $callback;
        }

        if(is_callable($closure)) {
            call_user_func_array($closure, array());
        }

        array_pop(self::$pattern_stack);
        //Clear controller
        array_pop(self::$controllers);
    }

    /**
     * Since 4.0
     * route() - Routing with Callback/Controller
     * @param string $pattern Regex Pattern
     * @param array|Closure $callback Callback/ Array with 'controller' and 'callback'
     * @param string $closure
     * @param array|string $regex_syntax
     * @param string $method  The method used in this route
     */
    public static function route($pattern, $callback, $closure = '', $regex_syntax = '[\w-]+?', $method = null)
    {
        //Push parent pattern into stack

        $preg_pattern = self::getPregPattern($pattern, $regex_syntax);

        if(!empty(ltrim(rtrim($preg_pattern, '/')))){
            array_push(self::$pattern_stack, ltrim(rtrim($preg_pattern, '/'), '/'));
        }else{
            array_push(self::$pattern_stack, '');
        }

        //Filter root route inside parent
        $pattern_stack_without_root_dir = array_filter(self::$pattern_stack);
        $preg_pattern = '/' .
            (!empty(self::$pattern_stack)? implode(DIRECTORY_SEPARATOR , $pattern_stack_without_root_dir ) . '/' : '');

        //Handle {} brackets and remove all ()
        preg_match_all("/{(.*?)}\/$/", $pattern, $matches);
        foreach($matches[1] as $match_item) {
            array_push(self::$query_vars, $match_item);
        }

        //Push controller to array
        if(is_array($callback)){
            array_push(self::$controllers, $callback);
        }
        $preg_pattern =  '/^' . str_replace('/', '\/', BASE_PATH . $preg_pattern) . '$/';

        self::$routes[$preg_pattern] = array(
            'controller' => self::flattenControllerArr(self::$controllers) ,
            'closure' => !is_array($callback) ? $callback : $closure,
            'pattern' => $pattern,
            'variable' => self::$query_vars,
            'method' => $method,
        );
        array_pop(self::$pattern_stack);
        //Pop controllers
        array_pop(self::$controllers);
    }

    /**
     * Since 4.0
     * post() - Handling post request only, an alis as route but add restriction to post only
     * @param string $pattern Regex Pattern
     * @param array|Closure $callback Callback/ Array with 'controller' and 'callback'
     * @param string $closure
     * @param array|string $regex_syntax
     */

    public static function post($pattern, $callback, $closure = '', $regex_syntax = '[\w-]+?')
    {
        self::route($pattern, $callback, $closure, $regex_syntax, 'POST');
    }

    public static function get($pattern, $callback, $closure = '', $regex_syntax = '[\w-]+?')
    {
        self::route($pattern, $callback, $closure, $regex_syntax, 'GET');
    }


    public static function execute($url)
    {
        $requesting_incorrect_method = false;
        foreach (self::$routes as $pattern => $item)
        {
            /**
             * Skip request and set the flag when the request method aren't right.
             * If we can't find a correct route at the end, we will output
             * 405 (Method not allowed)
             *
             */
            if(!empty($item['method']) && $item['method'] !== $_SERVER['REQUEST_METHOD']){
                $requesting_incorrect_method = true;
                continue;
            }
            if (preg_match($pattern, $url, $params)) {
                //Set query vars
                foreach ($item['variable'] as $index => $variable){
                    Request::add_query_var($variable, $params[$index + 1]);
                }
                $controller_passed = false;
                array_shift($params);
                if(count($params) > 0){
                    $params = array_combine($item['variable'], $params);
                }
                //Call controller if it's passed
                if(!empty($item['controller']) && is_array($item['controller']))
                {
                    //Validate user requests
                    foreach($item['controller'] as $func)
                    {
                        $class_func_arr = explode('@', $func);
                        $class_name = 'Controller\\' . $class_func_arr[0];
                        $class_obj = new $class_name();

                        if(!method_exists($class_obj, $class_func_arr[1])) throw new \Exception('Method doesn\'t exists');
                        if(!call_user_func([$class_obj, $class_func_arr[1]])){
                            throw new \Exception('Unable to pass controller check.');
                        }
                    }
                }
                if(is_callable($item['closure'])){
                    call_user_func($item['closure']);
                }

                exit;
            }
        }
        $data = array();
        $error_msg = ($requesting_incorrect_method === true) ? 'unaccepted_request_method' : 'page_not_found';
        $error_code = ($requesting_incorrect_method === true) ? 405 : 404;
        $data['error'] = $error_msg;
        $output = new OutputTools();
        $output->setData($data);
        $output->outputData('json', $error_code);
    }
}