<?php


namespace Labforum;


class Request
{
    protected static $query_vars = array();
    public static function add_query_var($key, $val = '')
    {
        self::$query_vars[$key] = $val;
    }

    public static function get_query_var($key): string
    {
        return (!empty(self::$query_vars[$key])) ? self::$query_vars[$key] : '' ;
    }

    public static function get_query_var_array(): array
    {
        return self::$query_vars;
    }
}