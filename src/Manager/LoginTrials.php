<?php

/**
 *  Boy, we are getting series about blind trials unless admin turns it off.
 *  If they are trying incorrect passwords for more than 3 times, the system will blacklist
 *  the IP Address for a certain time.
 */

namespace Manager;


use Controller\PollController;

class LoginTrials
{
    public $connection;
    public $ip_address;
    public static $max_login_trials = 3;

    public function __construct()
    {
        global $connection;
        $this->connection =& $connection;
        $this->ip_address = get_ip_address();
    }

    public function getIncorrectAttempts(){
        /**
         *  Get ALL Attempts, including those with or without userid.
         */
        $count = 0;
        $counter_arr = $this->connection->select(PREFIX . '_login_trials', 'trial', [
            'ip_address' => $this->ip_address,
        ]);

        foreach($counter_arr as $item){
            $count += $item;
        }
        return $count;
    }

    public function getLastTrialTime(){
        $time = 0;
        $last_trial_times = $this->connection->select(PREFIX . '_login_trials', 'updated_at', [
            'ip_address' => $this->ip_address,
        ]);

        foreach ($last_trial_times as $times_val){
            if(strtotime($times_val) > $time){
                $time = strtotime($times_val);
            }
        }
        return $time;
    }

    public function getRoadBlockRemainingTime(){
        $incorrect_counter = $this->getIncorrectAttempts();
        if(($this::$max_login_trials - $incorrect_counter) > 0) return 0;

        $roadblock_time = pow(2, abs($this::$max_login_trials - $incorrect_counter)) * 3600;
        $last_trial_time = $this->getLastTrialTime();
        return ($last_trial_time + $roadblock_time) - time();
    }

    public function incrementIncorrectAttempts($ip_address, $attempted_userid = null)
    {
        /**
         * Check if login trials table has attempt record and increment the number.
         * if not, add a new one.
         */
        $where_arr = array(
            'ip_address[=]' => $ip_address,
            'attempted_userid[=]' => ($attempted_userid) ? $attempted_userid : null,
        );

        $trials = $this->connection->get(PREFIX . '_login_trials', 'trial', $where_arr);

        if($trials){
            $this->connection->update(PREFIX . '_login_trials', [
                'trial' => ++$trials,
                'updated_at' => date('Y-m-d H:i:s'),
            ], $where_arr);
        }
        else{
            $this->connection->insert(PREFIX . '_login_trials', [
                'ip_address' => $this->ip_address,
                'attempted_userid' => !empty($attempted_userid) ? $attempted_userid: null,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }

}